# ABOUT ############################################################################################

# Back-end for filmsnow.co

# HISTORY ##########################################################################################

# First developed by Henry Morris in mid-2018
# Refactored and commented by Henry Morris on 6 October 2018

# IMPORTS  #########################################################################################

import flask  # to act as webserver
import requests  # to query films API
import pandas  # to manipulate data
import datetime  # to understand times

# MAKE FLASK APP ###################################################################################
app = flask.Flask(__name__)

# DEFINE FUNCTIONS #################################################################################


def mins_left_calc(input_datetime):
    """Returns minutes between time now and input datetime"""
    # Calculate difference between input time and time now as datetime.timedelta object
    difference = input_datetime - datetime.datetime.now()
    # Save difference as minutes
    difference = datetime.timedelta.total_seconds(difference) / 60
    # Round difference to 0 decimal places
    difference = int(difference)
    # Return difference
    return difference


def single_cinema_id_listings(cinema_id):
    """Returns pandas dataframe of listings for input cinema ID"""
    # Save raw json from cinelist.co.uk API for defined cinema
    # Do not verifying SSL given issues with cinelist.co.uk's certificate
    single_cinema_id_listings = requests.get(
        f"https://api.cinelist.co.uk/get/times/cinema/{cinema_id}", verify=False
    ).json()
    # If no listings can be found
    if "listings" not in single_cinema_id_listings:
        # Return empty dataframe
        return pandas.DataFrame(
            columns=["title", "scheduled_time", "datetime", "estimated_time", "mins_left"]
        )
    # Save just listings from raw json
    single_cinema_id_listings = single_cinema_id_listings["listings"]
    # Convert listings to a pandas dataframe
    single_cinema_id_listings = pandas.DataFrame.from_dict(single_cinema_id_listings)
    # Expand listing into seperate rows
    single_cinema_id_listings = (
        single_cinema_id_listings.set_index(
            [x for x in single_cinema_id_listings.columns if x != "times"]
        )["times"]
        .apply(pandas.Series)
        .stack()
        .reset_index()
    )
    # Drop temporary row
    single_cinema_id_listings.drop("level_1", axis=1, inplace=True)
    # Rename time column
    single_cinema_id_listings = single_cinema_id_listings.rename(
        columns={single_cinema_id_listings.columns[-1]: "scheduled_time"}
    )
    # Reset listings index
    single_cinema_id_listings = single_cinema_id_listings.reindex()
    # Add datetime column
    single_cinema_id_listings["datetime"] = pandas.to_datetime(
        single_cinema_id_listings["scheduled_time"]
    )
    # Add estimated_time column 20 minutes ahead of scheduled_time
    single_cinema_id_listings["estimated_time"] = single_cinema_id_listings[
        "datetime"
    ] + datetime.timedelta(minutes=20)
    # Add mins_left column
    single_cinema_id_listings["mins_left"] = single_cinema_id_listings["estimated_time"].apply(
        mins_left_calc
    )
    # Format estimated_time to be HH:MM
    single_cinema_id_listings["estimated_time"] = single_cinema_id_listings[
        "estimated_time"
    ].dt.strftime("%H:%M")
    # Return listings dataframe
    return single_cinema_id_listings


def mutiple_cinema_ids_listings(cinema_ids):
    """Returns pandas dataframe of listings for input cinema IDs"""
    # Create pandas dataframe with required columns
    mutiple_cinema_ids_listings = pandas.DataFrame(
        data={"title": [], "scheduled_time": [], "mins_left": [], "estimated_time": []}
    )
    # For each input cinema ID
    for cinema_id in cinema_ids:
        # Save listings using cinema ID
        single_cinema_id_listings_temp = single_cinema_id_listings(cinema_id[1])
        # Add cinema column to listings
        single_cinema_id_listings_temp["cinema"] = cinema_id[0]
        # Append cinema titles and times to bottom of titles_times dataframe
        mutiple_cinema_ids_listings = pandas.concat(
            [single_cinema_id_listings_temp, mutiple_cinema_ids_listings], sort=True
        )
    # Reset listings index
    mutiple_cinema_ids_listings.reset_index(drop=True, inplace=True)
    # Make time column integer
    mutiple_cinema_ids_listings["mins_left"] = mutiple_cinema_ids_listings["mins_left"].astype(int)
    # Re-order listings
    mutiple_cinema_ids_listings = mutiple_cinema_ids_listings[
        ["title", "cinema", "mins_left", "scheduled_time", "estimated_time"]
    ]
    mutiple_cinema_ids_listings.sort_values(by=["mins_left"], ascending=False, inplace=True, axis=0)
    # Return list of titles and times
    return mutiple_cinema_ids_listings


def mutiple_cinema_ids_listings_html(cinema_ids_and_names):
    """Returns raw HTML of current and future listings for input cinema IDs"""
    # Create table next films
    output = mutiple_cinema_ids_listings(cinema_ids_and_names)
    # Remove films that have already started
    output = output.query("mins_left >= 0")
    # Rename columns for ease of reading in the browser
    output.rename(
        columns={
            "mins_left": "Time to estimated",
            "title": "Film",
            "cinema": "Cinema",
            "scheduled_time": "Scheduled",
            "estimated_time": "Estimated",
        },
        inplace=True,
    )
    # Convert minutes to hours and remainder minutes
    output["minutes"] = (output["Time to estimated"] % 60).astype(str)
    output["hours"] = ((output["Time to estimated"] / 60).astype(int)).astype(str)
    # Overwrite column with formatted countdown
    output["Time to estimated"] = output["hours"] + " hours, " + output["minutes"] + " minutes"
    # Adjust films about to start
    output["Time to estimated"] = output["Time to estimated"].str.replace(
        r"\b0 minutes\b", "This minute", regex=True
    )
    # Adjust films due to start in exactly one minute
    output["Time to estimated"] = output["Time to estimated"].replace("1 minutes", "1 minute")
    # Adjust films due to start in one hour or more
    output["Time to estimated"] = output["Time to estimated"].str.replace(
        r"\b1 hours\b", "1 hour", regex=True
    )
    # Adjust films due to start in less than an hour
    output["Time to estimated"] = output["Time to estimated"].str.replace(
        r"\b0 hours, \b", "", regex=True
    )
    # Adjust films due to start in exacly a mutiple of an hour
    output["Time to estimated"] = output["Time to estimated"].str.replace(
        r"\b, 0 minutes\b", "", regex=True
    )
    # Remove temporary columns used for countdown column
    output.drop(["minutes", "hours"], axis=1, inplace=True)
    # Return dataframe as HTML
    return output.to_html(index=False, border="0", classes="display compact")


# DEFINE ROUTES ####################################################################################


@app.route("/")
def home():
    """Returns formatted HTML to browser of listings for hard coded cinema IDs"""
    output = mutiple_cinema_ids_listings_html(
        {
            ("Tottenham Court Road", 9868),
            ("Covent Garden", 9869),
            ("Haymarket", 10682),
            ("Leicester Square", 10716),
        }
    )
    return flask.render_template("index.html", output=output)


# END OF FILE ######################################################################################
